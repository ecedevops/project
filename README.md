# AAManager

[![forthebadge](https://gitlab.com/ecedevops/project/badges/master/pipeline.svg)](http://forthebadge.com)

- URL du projet : https://gitlab.com/ecedevops/project
- URL de la platforme de test : http://devopsproject.veryloop.fr
- Image docker (nginx) : [ecedevops/nginxfpmhydraapp](https://hub.docker.com/r/ecedevops/nginxfpmhydraapp)
- Image docker (app/php-fpm) : [ecedevops/symfonyhydraapp](https://hub.docker.com/r/ecedevops/symfonyhydraapp)
- Les conteneurs sont hebergés sur une grappe k8s google gratuite
- Dashboard Traefik : http://ecedashboard.veryloop.fr/dashboard/

Traefik a été déployé avec Helm3 depuis un shell google

```bash
helm install traefik traefik/traefik -f .k8s/traefik_values.yaml
kubectl apply -f .k8s/traefik.yaml
kubectl apply -f .k8s/traefik_dashboard.yaml
kubectl apply -f .k8s/traefik_middleware.yaml
```


Service de gestion de contenu. Application de gestion d'Articles et Auteurs.
Créer, modifier et supprimé vos contenus (Article / Auteur) depuis le site web (HTML) ou l'API REST.

### Pré-requis

- Docker
- PHP 7.4
- Composer (1 ou 2)

### Installation

```bash
docker-compose up -d
php bin/console doctrine:migrations:migrate
php bin/console fos:elastica:populate
```


Ensuite vous pouvez montrer ce que vous obtenez au final...

## Démarrage

```bash
cd public
php -S 0.0.0.0:8000
```

## Fabriqué avec

* [Symfony 5](https://symfony.com) - Framework PHP (backend)
* [MariaDB](https://mariadb.com) - SGBD OpenSource
* [Elasticsearch](https://www.elastic.co/fr/elasticsearch/) - Moteur de recherche

## Contributing

Si vous souhaitez contribuer, faite une pull request.

## Versions

**Dernière version :** 1.0.0.beta-1

## Auteurs

* **Maxime Chartier**
* **Tristan Aymé-Martin**
* **Nazim Bensouiki**


## License

Ce projet est sous licence MIT
