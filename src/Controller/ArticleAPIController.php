<?php

namespace App\Controller;

use App\Entity\Article;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use FOS\ElasticaBundle\HybridResult;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;

class ArticleAPIController extends AbstractController
{

    /**
     * @Route("/api/articles/search", name="api_article_search", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns articles",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Article::class))
     *     )
     * )
     * @OA\Parameter(
     *     name="q",
     *     in="query",
     *     description="Query",
     *     @OA\Schema(type="string")
     * )
     * @OA\Tag(name="Article")
     * @param Request $request
     * @param TransformedFinder $articlesFinder
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function searchDocument(Request $request, TransformedFinder $articlesFinder, SerializerInterface $serializer)
    {
        $q = $request->get('q');
        $results = !empty($q) ? $articlesFinder->findHybrid($q) : [];

        $transform = function(HybridResult $value) {
            return $value->getTransformed();
        };

        $results = array_map($transform, $results);


        $json = $serializer->serialize($results, JsonEncoder::FORMAT, array_merge(
            [
                'json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS
            ]
        ));

        return new JsonResponse($json, 200, [], true);
    }
}
