#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install wget git libzip-dev unzip -yqq

wget https://getcomposer.org/installer
php installer --install-dir=/usr/local/bin/ --filename=composer
rm installer

docker-php-ext-install pdo pdo_mysql zip # add other extension needed 